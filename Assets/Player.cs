﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int OpacityCount = 0;
    public string DisplayAzulCurrent;
    public bool DisplayAzulSolved = false;
    public string DisplayGrecCurrent;
    public bool DisplayGrecSolved = false;
    public int sala = 1;
    public int lado = 3;
    public bool EspelhoActive = false;
    public bool InTransition = false;
    public int timer = 0;
    public bool DentroEspelho = false; 
    public Text VisorAzul;
    public int ItemSelecionado = 0;
    public Text VisorGrec;
    public int[] Inventario = {0,2,0,0,0,0,0};
    public GameObject[] Slots = new GameObject[7];
    public Sprite[] Itens = new Sprite[3];
    public GameObject[] Salas = new GameObject[11];
    public GameObject[,] Lados = new GameObject[11, 4];
    GameObject ObjEspelhoSuperficial;
    SpriteRenderer ObjTransition;
    Color Opacity(int m) => new Color(0, 0, 0, m * 0.0317f);

    void Start()
    {
        int cont;
        for (int i = 0; i < Salas.Length; i++)
        {
            cont = 0;
            foreach (Transform child in Salas[i].transform)
            {
                Lados[i, cont] = child.gameObject;
                cont++;
            }
        }
    }

    void SelecionarItem(int item)
    {
        ItemSelecionado = item;
    }

    void Awake()
    {
        Start();
        ObjTransition = GameObject.FindWithTag("Transition").GetComponent<SpriteRenderer>();
        ObjEspelhoSuperficial = GameObject.FindWithTag("FiltroEspelho");
        ObjTransition.color = Opacity(0);
        AtualizarSala();
    }


    void Update()
    {
        for (int i = 0; i < Slots.Length; i++)
        {
            if (Inventario[i]!=0)
            {
                Slots[i].GetComponent<SpriteRenderer>().sprite = Itens[Inventario[i]];
                if (i == ItemSelecionado - 1)
                {
                    Slots[i].GetComponent<SpriteRenderer>().color = new Color(46, 46, 46);
                }
            }
        }

        if (timer <= 63 && InTransition == true)
        {
            timer += 1;
            ObjTransition.color = Opacity(OpacityCount);

            if (timer >= 32)
            {
                OpacityCount -= 1;
                if (timer == 32)
                {
                    AtualizarSala();
                }
            }
            else
            {
                OpacityCount += 1;
            }
        }

        if (timer > 63)
        {
            timer = 0;
            InTransition = false;
            OpacityCount = 0;
        }
    }


    //aqui vão ser as funções dos botões

    public void EspelhoT()
    {
        if (EspelhoActive == true)
        {
            DentroEspelho = true;
            sala = 7;
            InTransition = true;
        }
    }

    public void EspelhoF()
    {
        DentroEspelho = false;
        sala = 1;
        InTransition = true;
    }

    public void Seta(int direc)
    {
        if (!InTransition)
        {
            InTransition = true;
            lado += direc;
            lado=lado==5?1:lado==0?4:lado;
        }
    }

    //portas
    public void door(int sala)
    {
        InTransition = true;
        this.sala = sala;
    }


    //display numérico

    public void Press(int num)
    {
        if (DisplayAzulCurrent.Length < 4 && DisplayAzulSolved == false)
        {
            DisplayAzulCurrent += num;
            VisorAzul.text = DisplayAzulCurrent;
        }
    }
    public void PressConfirm()
    {
        DisplayAzulSolved = DisplayAzulCurrent == "3238";
        DisplayAzulCurrent = "";
        VisorAzul.text = DisplayAzulCurrent;
    }
    public void PressCancel()
    {
        DisplayAzulCurrent = "";
        VisorAzul.text = DisplayAzulCurrent;
    }



    public void PressGrec(int num)
    {
        string[] grego = { "Ԅ", " ҈", "Ϫ", "ϛ", "ϡ", "Ѿ", "Ѧ", "֎", "ξ", "Θ" };
        if (DisplayGrecCurrent.Length < 6 && DisplayGrecSolved == false)
        {
            DisplayGrecCurrent += grego[num];
            VisorGrec.text = DisplayGrecCurrent;
        }
    }
    public void PressConfirmGrec()
    {
        DisplayGrecSolved = DisplayGrecCurrent == "ѦԄԄ֎ΘϪ";
        DisplayGrecCurrent = "";
        VisorGrec.text = DisplayGrecCurrent;
    }
    public void PressCancelGrec()
    {
        DisplayGrecCurrent = "";
        VisorGrec.text = DisplayGrecCurrent;
    }


    void AtualizarSala()
    {
        ObjEspelhoSuperficial.SetActive(DentroEspelho);

        for (int i = 0; i < Salas.Length; i++)
        {
            Salas[i].SetActive(false);
            for (int j = 0; j < 4; j++)
            {
                Lados[i, j].SetActive(false);
            }
        }
        Salas[(sala-1)].SetActive(true);
        Lados[(sala-1), (lado-1)].SetActive(true);
    }
}